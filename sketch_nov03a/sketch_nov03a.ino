#define BLACK 0

const int PLeftFront = 3;
const int PLeftBack = 2;
const int PRightFront = 5;
const int PRightBack = 4;
const int PirLed[] = {9, 10, 11, 12, 13};
const int forwardTime = 200;
const int turnTime = 500;

const int rotationTime = 500;

int curIR[5] = {0, 0, 0, 0, 0};

void setup() {
  for (int i = 0; i < 5; i++) {
    pinMode(PirLed[i], INPUT);
  }
  pinMode(PLeftFront, OUTPUT);
  pinMode(PLeftBack, OUTPUT);
  pinMode(PRightFront, OUTPUT);
  pinMode(PRightBack, OUTPUT);
}

void statusQuo() {
  if (curIR[0] == BLACK && curIR[4] != BLACK) {
    left();
  }
  if (curIR[4] == BLACK && curIR[0] != BLACK) {
    right();
  }
  if (curIR[4] != BLACK && curIR[0] != BLACK) {
    forward();
  }
}

void forward() {
  digitalWrite(PLeftFront, 1);
  digitalWrite(PLeftBack, 0);
  digitalWrite(PRightFront, 1);
  digitalWrite(PRightBack, 0);
}

void backward() {
  digitalWrite(PLeftFront, 0);
  digitalWrite(PLeftBack, 1);
  digitalWrite(PRightFront, 0);
  digitalWrite(PRightBack, 1);
}

void left() {
  digitalWrite(PLeftFront, 0);
  digitalWrite(PLeftBack, 1);
  digitalWrite(PRightFront, 1);
  digitalWrite(PRightBack, 0);
}

void right() {
  digitalWrite(PLeftFront, 1);
  digitalWrite(PLeftBack, 0);
  digitalWrite(PRightFront, 0);
  digitalWrite(PRightBack, 1);
}

void irRead() {
  for (int i = 0; i < 8; i++) {
    curIR[i] = digitalRead(PirLed);
  }
}

bool detectIntersection() {
  if (curIR[0] == curIR[1] == curIR[2] == curIR[3] == curIR[4] == BLACK)
    return true;
  else
    return false;
}

int intersections = 0;

void loop() {
  // if (detectIntersection()) {
  //   Serial.write("Detected intersection");
  //   intersections++;
  //   if (intersections == 0) {
  //     forward();
  //     delay(forwardTime);
  //     left();
  //     delay(turnTime);
  //   } else if (intersections == 1) {
  //     forward();
  //     delay(forwardTime);
  //     right();
  //     delay(turnTime);
  //   }
  // }
  statusQuo();
}
